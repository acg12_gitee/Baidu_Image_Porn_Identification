﻿using System.Collections.Generic;

namespace CatUtils.Utils.SpecialUtils.ContentRecognition.BaiduUtils.ImageRecognition
{
#pragma warning disable CS1591 // 缺少对公共可见类型或成员的 XML 注释

    public class BaiduJson

    {
        public class ResultItem
        {
            /// <summary>
            /// 性感
            /// </summary>
            public string class_name { get; set; }

            /// <summary>
            ///
            /// </summary>
            public double probability { get; set; }
        }

#pragma warning disable S101 // Types should be named in PascalCase

        public class Result_fineItem
#pragma warning restore S101 // Types should be named in PascalCase
        {
            /// <summary>
            /// 一般色情
            /// </summary>
            public string class_name { get; set; }

            /// <summary>
            ///
            /// </summary>
            public double probability { get; set; }
        }

        public class Root
        {
            /// <summary>
            ///
            /// </summary>
            public int result_num { get; set; }

            /// <summary>
            /// 确定
            /// </summary>
            public string confidence_coefficient { get; set; }

            /// <summary>
            ///
            /// </summary>
            public List<ResultItem> result { get; set; }

            /// <summary>
            ///
            /// </summary>
            public List<Result_fineItem> result_fine { get; set; }

            /// <summary>
            /// 性感
            /// </summary>
            public string conclusion { get; set; }
        }

#pragma warning restore CS1591 // 缺少对公共可见类型或成员的 XML 注释
    }
}