﻿using Baidu.Aip.ContentCensor;
using CatUtils.Utils.Currency;
using CatUtils.Utils.Currency.IoUtils;
using CatUtils.Utils.Currency.TextUtils;
using CatUtils.Utils.Helper;
using System;
using System.Collections.Generic;
using static CatUtils.Utils.SpecialUtils.ContentRecognition.BaiduUtils.ImageRecognition.BaiduJson;

namespace CatUtils.Utils.SpecialUtils.ContentRecognition.BaiduUtils.ImageRecognition
{
    /// <summary>
    /// 百度帮助
    /// </summary>
    public class BaiduImgHelper
    {
        /// <summary>
        /// API_KEY保存路径
        /// </summary>
        public readonly static string API_KEY_Path = StaticValue.BinPath + "APIKEY.txt";

        /// <summary>
        /// SECRET_KEY保存路径
        /// </summary>
        public readonly static string SECRET_KEY_Path = StaticValue.BinPath + "SECRETKEY.txt";

        /// <summary>
        /// 设定可以运行的最大次数
        /// </summary>
        public readonly static string MaxCountSetPath = StaticValue.BinPath + "MAXCount.txt";

        /// <summary>
        /// 本地验证 一次初始化
        /// </summary>
        public static AntiPorn LocalAntiPorn = null;

        /// <summary>
        /// 记录已经使用了多少次
        /// </summary>
        public static string FrequencyRecord = StaticValue.BinPath + DateTime.Now.ToString("yyyyMMdd") + "BaiduSexCountFile.txt";

        /// <summary>
        /// 配置最大使用次数
        /// </summary>
        public static int MaxCount = InitCount();

        /// <summary>
        /// 需要识别的列表
        /// </summary>
        public static List<Result_fineItem> result_fine = new List<Result_fineItem>() {
            new Result_fineItem{  class_name="卡通女性性感", probability=0.95},
                  new Result_fineItem{  class_name="卡通色情", probability=0.8},
                        new Result_fineItem{  class_name="卡通亲密行为", probability=0.9}
        };

        /// <summary>
        /// 异常情况下，直接停掉程序
        /// </summary>
        public static void SetMaxCount()
        {
            FileHelper.WriteText(FrequencyRecord, MaxCount.ToString());
        }

        /// <summary>
        /// 查看是否超出了极限
        /// </summary>
        /// <param name="CountAdd">如果为false 则减去一次</param>
        /// <returns></returns>
        public static bool GetJumpFlag(bool CountAdd)
        {
            int FileCount = TextHelper.StringToInt(FileHelper.ReadAllText(FrequencyRecord));
            if (FileCount >= MaxCount)
            {
                Console.WriteLine($"当前：{FileCount}次数，已经超每日鉴黄：{MaxCount}次数，跳出！");
                return true;
            }
            if (CountAdd)
                FileHelper.WriteText(FrequencyRecord, ++FileCount + "");
            else FileHelper.WriteText(FrequencyRecord, --FileCount + "");
            return false;
        }

        /// <summary>
        /// 初始化程序运行次数
        /// </summary>
        /// <returns></returns>
        private static int InitCount()
        {
            if (!FileHelper.FileExits(MaxCountSetPath))
            {
                FileHelper.WriteText(MaxCountSetPath, 1950.ToString());
            }

            return TextHelper.StringToInt(FileHelper.ReadAllText(MaxCountSetPath));
        }
    }
}