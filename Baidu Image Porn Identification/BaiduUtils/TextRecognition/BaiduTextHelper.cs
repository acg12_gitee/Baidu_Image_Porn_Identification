﻿using Baidu.Aip.ContentCensor;
using Baidu.Aip.Nlp;
using CatUtils.Utils.Currency;
using CatUtils.Utils.Enhance.ThreadTool;
using CatUtils.Utils.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatUtils.Utils.SpecialUtils.ContentRecognition.BaiduUtils.TextRecognition
{
    internal class BaiduTextHelper
    {
        /// <summary>
        /// API_KEY保存路径
        /// </summary>
        public readonly static string API_KEY_Path = StaticValue.BinPath + "TextAPIKEY.txt";

        /// <summary>
        /// SECRET_KEY保存路径
        /// </summary>
        public readonly static string SECRET_KEY_Path = StaticValue.BinPath + "TextSECRETKEY.txt";

        /// <summary>
        /// 设定可以运行的最大次数
        /// </summary>
        public readonly static string MaxCountSetPath = StaticValue.BinPath + "MAXCount.txt";

        /// <summary>

        /// 本地验证 一次初始化
        /// </summary>
        public static Nlp LocalNlp = null;

        /// <summary>
        ///语言正负能量判断
        /// </summary>
        /// <param name="TextContent"></param>
        /// <returns></returns>
        public static NlpRoot SentimentClassify(string TextContent)
        {
            var client = BaiduCurrencyHelper.GetNPLClient(LocalNlp, API_KEY_Path, SECRET_KEY_Path);
            if (TextContent == "" || client == null)
                return null;

            try
            {
                // 调用情感倾向分析，可能会抛出网络等异常，请使用try/catch捕获
                var result = client.SentimentClassify(TextContent).ToString();
                Console.WriteLine(result);
                if (result.IndexOf("Open api qps request limit reached") >= 0)
                {
                    PrintLog.ENoWrite("QPS速率过高，休眠 10 秒后重试！");
                    ThreadSleep.SetSecondClock(10);
                    return SentimentClassify(TextContent);
                }

                NlpRoot nlp = JsonConvert.DeserializeObject<NlpRoot>(result.ToString());
                Console.WriteLine("测试文本:" + nlp.text);
                foreach (var item in nlp.items)
                {
                    Console.WriteLine("积极程度：" + item.positive_prob);
                    Console.WriteLine("消极程度：" + item.negative_prob);
                }
            }
            catch (Exception ex)
            {
                PrintLog.E(ex);
            }
            return null;
        }

        public class ItemsItem
        {
            /// <summary>
            ///
            /// </summary>
            public double positive_prob { get; set; }

            /// <summary>
            ///
            /// </summary>
            public double confidence { get; set; }

            /// <summary>
            ///
            /// </summary>
            public double negative_prob { get; set; }

            /// <summary>
            ///
            /// </summary>
            public int sentiment { get; set; }
        }

        public class NlpRoot
        {
            /// <summary>
            ///
            /// </summary>
            public long log_id { get; set; }

            /// <summary>
            ///
            /// </summary>
            public string text { get; set; }

            /// <summary>
            ///
            /// </summary>
            public List<ItemsItem> items { get; set; }
        }
    }
}