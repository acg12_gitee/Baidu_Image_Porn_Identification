﻿using Baidu.Aip.ContentCensor;
using Baidu.Aip.Nlp;
using CatUtils.Utils.Currency;
using CatUtils.Utils.Currency.IoUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatUtils.Utils.SpecialUtils.ContentRecognition.BaiduUtils
{
    /// <summary>
    /// 百度通用工具类
    /// </summary>
    public class BaiduCurrencyHelper
    {
        /// <summary>
        /// 保存信息
        /// </summary>
        /// <param name="API_KEY_Path"></param>
        /// <param name="SECRET_KEY_Path"></param>
        /// <param name="ApiKey"></param>
        /// <param name="SecretKey"></param>
        /// <param name="LocalAntiPorn"></param>
        /// <returns></returns>
        public static bool SaveImgInfo(string API_KEY_Path, string SECRET_KEY_Path, string ApiKey, string SecretKey, AntiPorn LocalAntiPorn)
        {
            if (ApiKey == "" || SecretKey == "")
            {
                PrintLog.E("SecretKey或APIKEY不能为空！");
                return false;
            }
            FileHelper.WriteText(API_KEY_Path, ApiKey);
            FileHelper.WriteText(SECRET_KEY_Path, SecretKey);
            LocalAntiPorn = CreateImgClient(API_KEY_Path, SECRET_KEY_Path);
            return true;
        }

        /// <summary>
        /// 创建辅助方法类
        /// </summary>
        /// <returns></returns>
        public static AntiPorn CreateImgClient(string API_KEY_Path, string SECRET_KEY_Path)
        {
            string ApiKey = FileHelper.ReadAllText(API_KEY_Path);
            string SecretKey = FileHelper.ReadAllText(SECRET_KEY_Path);
            if (ApiKey == "" || SecretKey == "")
            {
                System.Console.WriteLine("请开发者配置密钥。");
                return null;
            }
            AntiPorn client = new AntiPorn(FileHelper.ReadAllText(API_KEY_Path), FileHelper.ReadAllText(SECRET_KEY_Path))
            {
                Timeout = 60000  // 超时，毫秒
            };

            return client;
        }

        /// <summary>

        /// 获取并初始化一个唯一的client 提高程序效率
        /// </summary>
        /// <returns></returns>
        public static AntiPorn GetImgClient(AntiPorn LocalAntiPorn, string API_KEY_Path, string SECRET_KEY_Path)
        {
            if (LocalAntiPorn == null)
            {
                LocalAntiPorn = CreateImgClient(API_KEY_Path, SECRET_KEY_Path);
            }
            return LocalAntiPorn;
        }

        /// <summary>
        /// 获取并初始化一个唯一的client 提高程序效率
        /// </summary>
        /// <returns></returns>
        public static Nlp GetNPLClient(Nlp LocalAntiPorn, string API_KEY_Path, string SECRET_KEY_Path)
        {
            if (LocalAntiPorn == null)
            {
                LocalAntiPorn = CreateNPLClient(API_KEY_Path, SECRET_KEY_Path);
            }
            return LocalAntiPorn;
        }       /// <summary>

        /// 创建辅助方法类
        /// </summary>
        /// <returns></returns>
        public static Nlp CreateNPLClient(string API_KEY_Path, string SECRET_KEY_Path)
        {
            string ApiKey = FileHelper.ReadAllText(API_KEY_Path);
            string SecretKey = FileHelper.ReadAllText(SECRET_KEY_Path);
            if (ApiKey == "" || SecretKey == "")
            {
                Console.WriteLine("请开发者配置密钥。");
                return null;
            }

            var client = new Nlp(FileHelper.ReadAllText(API_KEY_Path), FileHelper.ReadAllText(SECRET_KEY_Path))
            {
                Timeout = 60000  // 超时，毫秒
            };

            return client;
        }     /// <summary>

        /// 保存信息
        /// </summary>
        /// <param name="ApiKey"></param>
        /// <param name="SecretKey"></param>
        public static bool SaveNplInfo(string API_KEY_Path, string SECRET_KEY_Path, string ApiKey, string SecretKey, Nlp LocalNlp)
        {
            if (ApiKey == "" || SecretKey == "")
            {
                PrintLog.E("SecretKey或APIKEY不能为空！");
                return false;
            }
            FileHelper.WriteText(API_KEY_Path, ApiKey);
            FileHelper.WriteText(SECRET_KEY_Path, SecretKey);
            LocalNlp = CreateNPLClient(API_KEY_Path, SECRET_KEY_Path);
            return true;
        }
    }
}