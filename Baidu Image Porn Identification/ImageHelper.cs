﻿using CatUtils.Utils.Currency;
using CatUtils.Utils.Currency.IoUtils;
using System;
using System.Drawing.Imaging;

namespace CatUtils.Utils.SpecialUtils.Media
{
    /// <summary>
    /// 图片帮助 基础方法
    /// </summary>
    public class ImageHelper
    {
        /// <summary>
        /// 从文件夹运行删除缩略图，指定小于等于某个宽高即处理
        /// </summary>
        /// <param name="FloderPath"></param>
        /// <param name="MinWidth"></param>
        /// <param name="MinHeight"></param>
        public void DeleteLittleImageFloder(string FloderPath, int MinWidth, int MinHeight)
        {
            foreach (string line in FloderNameHelper.GetFullFileNameList(FloderPath))
            {
                DeleteLittleSinleImage(line, MinWidth, MinHeight);
            }
        }

        /// <summary>
        ///  删除缩略图，指定小于等于某个宽高即处理
        /// </summary>
        /// <param name="FilePath"></param>
        /// <param name="MinWidth"></param>
        /// <param name="MinHeight"></param>
        public void DeleteLittleSinleImage(string FilePath, int MinWidth, int MinHeight)
        {
            try
            {
                using (System.Drawing.Image localimage = System.Drawing.Image.FromFile(FilePath))
                {
                    Console.WriteLine("图片{0}，Width：{1}  Height{2}", FileNameHelper.GetFileName(FilePath), localimage.Width, localimage.Height);
                    if (localimage.Width > MinWidth && localimage.Height > MinHeight)
                    {
                        return;
                    }
                }
                Console.WriteLine("不符合规定，删除");
                FileHelper.FileDelete(FilePath);
            }
            catch (Exception ex)
            {
                PrintLog.Log(ex);
                PrintLog.Log(FilePath);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="SavePath"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public string MakeThumbnail(string sourcePath, string SavePath, int width, int height, ImageFormat imageFormat)
        {
            System.Drawing.Image ig = System.Drawing.Image.FromFile(sourcePath);
            int towidth = width;
            int toheight = height;
            int x = 0;
            int y = 0;
            int ow = 0;
            int oh = 0;
            if ((double)ig.Width / (double)ig.Height > (double)towidth / (double)toheight)
            {
                oh = ig.Height;
                ow = ig.Height * towidth / toheight;
                y = 0;
                x = (ig.Width - ow) / 2;
            }
            else
            {
                ow = ig.Width;
                oh = ig.Width * height / towidth;
                x = 0;
                y = (ig.Height - oh) / 2;
            }
            System.Drawing.Image bitmap = new System.Drawing.Bitmap(towidth, toheight);

            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);

            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.Clear(System.Drawing.Color.Transparent);

            g.DrawImage(ig, new System.Drawing.Rectangle(0, 0, towidth, toheight), new System.Drawing.Rectangle(x, y, ow, oh), System.Drawing.GraphicsUnit.Pixel);
            try
            {
                bitmap.Save(SavePath, imageFormat);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                ig.Dispose();
                bitmap.Dispose();
                g.Dispose();
            }

            return SavePath;
        }

        /// <summary>
        /// 图片显示成缩略图 返回缩略图缓存文件
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public string MakeThumbnail(string sourcePath, int width, int height)
        {
            return MakeThumbnail(sourcePath, FileNameHelper.CreateTempFileName(), width, height, ImageFormat.Png);
        }

        /// <summary>
        /// 图片显示成缩略图 返回缩略图缓存文件
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="imageFormat"></param>
        /// <returns></returns>
        public string MakeThumbnail(string sourcePath, int width, int height, ImageFormat imageFormat)
        {
            return MakeThumbnail(sourcePath, FileNameHelper.CreateTempFileName(), width, height, imageFormat);
        }
    }
}